Allow storing falsy values using the `storage.update` action.
